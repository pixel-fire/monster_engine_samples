﻿using System;
using System.IO;

using MonsterEngine.Engine;
using MonsterEngine.Input;
using MonsterEngine.Loaders;
using MonsterEngine.Loaders.Files;
using MonsterEngine.Primitives3D;
using MonsterEngine.Structs;

namespace Sample02_FileOperations.Stages
{
    class MainStage : Stage
    {
        public MainStage()
            : base("Main Stage", "stages.mainStage")
        {

        }

        protected override void StageStart(string[] args = null)
        {
            //===============================================================
            //  NB: All file operations albeit packages or loose files must be done in a Stage.StageStart function.
            //===============================================================

            //Packages are the preferred method of file loading.
            // They are loaded when the engine is loaded, which reduces loading time between stages.
            // They are structured like a folder.

            //To get a file from a package is easy.
            // 1. Just call Globals.MonsterEngine.Settings.GetPakFile, then specify the name of the package.
            // 2. Type Get{XXXX}ByName, than specify the name of the asset you want to load.
            //  * {XXXX} - the asset type: Audio, Model, Cubemap, Font, Texture, Shader.
            // 3. To convert a {XXXX}File to a usable asset just type .{XXXX};

            //In this example we get the "sample02" package and load the "example" texture.
            PakFile package = Globals.MonsterEngine.Settings.GetPakFile("sample02");
            Texture texture = package.GetTextureByName("example").Texture;

            //Now let's show the "example" texture.
            var cube = AddEntity("package_cube", new Cube(this));
            cube.SetMaterial(new Material(new Vector3(32), 32f, texture, texture));
            cube.Transform.SetPosition(new Vector3(0, 5, -3));
            cube.OnUpdate += Cube_OnUpdate;

            //Although packages are the preferred method of file loading, you can load models using ObjectFileLoader.
            // Using this static class you can create *.meModels, which are the way models are saved in the package.

            //The last parameter tells the function, should it save the new model
            // If it is true it will be saved in Data/Output/MeModels
            // Now you can just make an entity and add a MeshRenderer component and load the model.
            float[] modelData = ObjectFileLoader.GetMesh(Directory.GetCurrentDirectory() + "/Data/spaceShip.obj", false);

            //AudioFileLoader is the same as ObjectFileLoader.
            // The only difference is that it loads *.wav and *.mp3 files.
            // For audio usage see Sample04.
            AudioFileData data = AudioFileLoader.GetAudio(Directory.GetCurrentDirectory() + "/Data/audio.mp3", false);

            //======================================================================
            //                         Package Creation
            //======================================================================

            //We used the already unpackaged files but we can create and load new packages on the run.
            // The static class PakLoader has functions for packing and unpacking.
            // Here we have an example of a package creation.
            /*                  
             *             The name of the package                Path to the package contents
             *                      |                                       |
             *                      V                                       V
            PakLoader.Package("packageName.pak", new PakFile("pathToPackageContents",
            new List<ModelFile>
            {
                //the *.meModel go here
            },
            new List<TextureFile>
            {
                //the textures go here
            },
            new List<ShaderFile>
            {
                //the shaders go here
            },
            new List<FontFile>
            {
                //the fonts go here
            },
            new List<AudioFile>
            {
                //the *.meAudio go here
            }), 
            true); <-- should the created file be overwritten?
            */
        }

        private void Cube_OnUpdate(object sender, EventArgs e)
        {
            ((Cube)sender).Transform.AddRotation(MonsterEngine.Enums.Axis.XYZ, 30 * Globals.DeltaTime);
        }

        protected override void StageStop()
        {
            
        }

        protected override void StageUpdate()
        {
            if (Globals.MonsterEngine.IsKeyPressed(KeyboardKey.Escape))
                Globals.MonsterEngine.StopEngine();

            DoFreeCamMovementWithoutKeyPress();
        }
    }
}
