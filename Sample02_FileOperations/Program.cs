﻿using System.Collections.Generic;

using MonsterEngine.Engine;
using MonsterEngine.Engine.Settings;

using Sample02_FileOperations.Stages;

namespace Sample02_FileOperations
{
    class Program
    {
        static void Main(string[] args)
        {
            //Same code as in Sample00

            WindowSettings windowSettings = new WindowSettings(1600, 900, "Example 02 - File Operations", false);
            GraphicsSettings graphicsSettings = new GraphicsSettings(false, 100, 100);
            EngineSettings engineSettings = new EngineSettings(true, false);
            //The only change in this example is that we add "sample02.pak" to the engine.
            // See MainStage.cs!
            PluginsSettings pluginsSettings = new PluginsSettings(false, false, true, new List<string>()
            {
                "fonts.pak",
                "shaders.pak",
                "sample02.pak"
            });

            MasterSettings masterSettings = new MasterSettings(windowSettings, graphicsSettings, engineSettings, pluginsSettings);

            Engine engine = new Engine(masterSettings);

            engine.StartEngine(new List<Stage>()
            {
                new MainStage()
            });
        }
    }
}
