﻿using System;
using System.Collections.Generic;

using MonsterEngine.Engine;
using MonsterEngine.Engine.Settings;

using Sample05_Particles.Stages;

namespace Sample05_Particles
{
    class Program
    {
        static void Main(string[] args)
        {
            //Same code as in Sample00

            WindowSettings windowSettings = new WindowSettings(1600, 900, "Example 05 - Particles", false);
            GraphicsSettings graphicsSettings = new GraphicsSettings(false, 100, 100);
            EngineSettings engineSettings = new EngineSettings(true, false);
            PluginsSettings pluginsSettings = new PluginsSettings(false, false, true, new List<string>()
            {
                "fonts.pak",
                "shaders.pak"
            });

            MasterSettings masterSettings = new MasterSettings(windowSettings, graphicsSettings, engineSettings, pluginsSettings);

            Engine engine = new Engine(masterSettings);

            engine.StartEngine(new List<Stage>()
            {
                new MainStage()
            });
        }
    }
}
