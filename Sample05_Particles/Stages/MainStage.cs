﻿using System;
using System.Collections.Generic;
using System.Text;

using MonsterEngine.Engine;
using MonsterEngine.Input;
using MonsterEngine.Particles;
using MonsterEngine.Primitives3D;
using MonsterEngine.Structs;

namespace Sample05_Particles.Stages
{
    class MainStage : Stage
    {
        public MainStage()
            :base("Main Stage", "stages.MainStage")
        {

        }

        protected override void StageStart(string[] args = null)
        {
            //To create particles we need to create a Particle System.
            // But first we need a texture atlas for this system.
            var defaultPak = Globals.MonsterEngine.Settings.GetPakFile("default");
            var fireTexture = defaultPak.GetTextureByName("fire").Texture;
            var smokeTexture = defaultPak.GetTextureByName("smoke").Texture;

            //The Particle Texture class has 2 fields: a Texture and the number of rows/columns of the texture.
            // If the selected texture doesn't have rows/columns, set it to 1.
            ParticleTexture firePSTexture = new ParticleTexture(fireTexture, 8);
            ParticleTexture smokePSTexture = new ParticleTexture(smokeTexture, 8);

            //The particle system contains a couple of fields: 
            // *ID: the id of the PS - recommended format: "particleSystem.{identifier}"
            // *Texture: a ParticleTexture
            // *Particles Per Second: how particles should be emitted every second.
            // *Average Speed: the average speed of every particle.
            // *Average Life Span: the average life span in seconds of every particle.
            // *Average Scale: the average scale of every particle.
            // *Gravity Complaint: a number [0; 1], which specifies how much gravity affects the particles.
            //
            // *Center: the center of the particle system.
            // *Direction: the direction of the emitted particles - should be (1, 0, 0), (0, 1, 0), (0, 0, 1) or their negatives.
            // *Direction Deviation: how much a particle will deviate from it's direction.
            // *Speed Error: a number [0; 1], which indicates the error of the speed of a particle. If the number is 0 - not changed.
            // *Life Error: a number [0; 1], which indicates the error of the life of a particle. If the number is 0 - not changed.
            // *Scale Error: a number [0; 1], which indicates the error of the scale of a particle. If the number is 0 - not changed.
            var fire = ParticleManager.AddItem(new ParticleSystem("particleSystem.fire01", firePSTexture, 1000, 1f, 0.25f, 0.6f, 3f));
            fire.Center = new Vector3(2, -3, -4);
            fire.Direction = new Vector3(0, 1, 0);
            fire.DirectionDeviation = 1f;
            fire.LifeError = 0.15f;
            fire.SpeedError = 0.9f;
            fire.ScaleError = 0.63f;

            var smoke = ParticleManager.AddItem(new ParticleSystem("particleSystem.smoke01", smokePSTexture, 50, 0.25f, 0.6f, 4f, 10f));
            smoke.Center = new Vector3(-2, -3, -4);
            smoke.Direction = new Vector3(0, 1, 0);
            smoke.DirectionDeviation = 0.25f;
            smoke.LifeError = 0.15f;
            smoke.SpeedError = 0.9f;
            smoke.ScaleError = 0.63f;

            var cube = AddEntity("cube_smoke", new Cube(this));
            cube.Transform.SetPosition(new Vector3(-2, -3.5f, -4.15f));
        }

        protected override void StageStop()
        {

        }

        protected override void StageUpdate()
        {
            if (Globals.MonsterEngine.IsKeyPressed(KeyboardKey.Escape))
                Globals.MonsterEngine.StopEngine();

            DoFreeCamMovementWithoutKeyPress();
        }
    }
}
