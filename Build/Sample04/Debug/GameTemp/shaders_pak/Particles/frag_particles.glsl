#version 330 core

in vec2 textureCoords1;
in vec2 textureCoords2;
in float blend;

out vec4 fragColor;

uniform sampler2D particleTexture;

void main()
{
	vec4 col1 = texture(particleTexture, textureCoords1);
	vec4 col2 = texture(particleTexture, textureCoords2);

	fragColor = mix(col1, col2, blend);
}