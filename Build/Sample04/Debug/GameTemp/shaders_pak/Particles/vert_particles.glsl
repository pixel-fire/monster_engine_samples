#version 140

in vec2 position;
in mat4 model;
in vec4 texOffsets;
in float blendFactor;

out vec2 textureCoords1;
out vec2 textureCoords2;
out float blend;

uniform int _numberOfRows;
uniform mat4 projection;
//uniform mat4 view;

void main() {

	vec2 textureCoords = position + vec2(0.5, 0.5);
	textureCoords.y = 1.0 - textureCoords.y;
	
	textureCoords /= _numberOfRows;
	
	textureCoords1 = textureCoords + texOffsets.xy;
	textureCoords2 = textureCoords + texOffsets.zw;
	
	blend = blendFactor;
	
	gl_Position = projection * model * vec4(position, 0.0, 1.0);
}