﻿using System;
using System.Collections.Generic;
using System.Text;

using MonsterEngine.Engine;
using MonsterEngine.Enums;
using MonsterEngine.Input;
using MonsterEngine.Multimedia;
using MonsterEngine.Structs;

namespace Sample04_Managers.Stages
{
    class AudioStage : Stage
    {
        public AudioStage()
            :base("Audio Stage", "stages.audioStage")
        {

        }

        protected override void StageStart(string[] args = null)
        {
            //Audio is similar to GUIs.
            // First we need an audio file. In this example it is in the "audio.pak" with the name "music".
            var audioPak = Globals.MonsterEngine.Settings.GetPakFile("audio");
            var audioData = audioPak.GetAudioByName("music").AudioData;

            //Now we set the Distance Model - this is how should the audio fade when the source's threshold is reached.
            AudioManager.SetDistanceModel(AudioDistanceModel.LinearDistance);
            //We create a new audioSource and add it to the manager.
            var source = AudioManager.AddItem(new AudioSource(this, "Audio Source 1", "audioSources.audioSource1", 
                new Vector3(4, 3, 5), new Vector3(1, 0, 0), new Vector3(0, 1, 0), new AudioSourceData(audioData)));
            //We set the threshold of the source and it's volume.
            source.SetRolloffDistance(2.5f);
            source.SetVolume(0.1f);

            //Finally we play the source and loop it.
            source.Play();
            source.Loop(true);
        }

        protected override void StageStop()
        {
            
        }

        protected override void StageUpdate()
        {
            if (Globals.MonsterEngine.IsKeyPressed(KeyboardKey.Escape))
                Globals.MonsterEngine.StopEngine();
            if (Globals.MonsterEngine.IsKeyPressed(KeyboardKey.D1))
                Globals.MonsterEngine.SetStage("stages.guiStage");

            DoFreeCamMovementWithoutKeyPress();
        }
    }
}
