﻿using System;

using ImGuiNET;

using MonsterEngine.Engine;
using MonsterEngine.ImGui.Widgets;
using MonsterEngine.Input;
using MonsterEngine.Structs;

namespace Sample04_Managers.Stages
{
    class GUIStage : Stage
    {
        //We declare some variables that will come in handy.
        int m_timesClicked = 0;
        int m_cooldown = 0;
        Text m_text;

        public GUIStage()
            :base("Main Stage", "stages.guiStage")
        {

        }

        protected override void StageStart(string[] args = null)
        {
            //This example is focused on Managers.
            // They are a handy tool for managing a lot of objects at once.
            // In the stage there are 3 managers: Audio Manager, GUI Manager and Particle Manager.
            // The Particle Manager is shown in example 05.


            //There are 2 ways to draw GUI to the screen.
            // The first(advanced) way is to use Dear ImGui.
            // Just use the OnImGuiRender event in the Engine class.
            // On every render the GUI will be redrawn.
            // For more info about creating windows, docking and widgets see: https://github.com/ocornut/imgui/blob/master/imgui_demo.cpp
            // The C# functions are the same as in C++.
            // The current version of the implementation of ImGui in MonsterEngine is 1.82.
            Globals.MonsterEngine.OnImGuiRender += MonsterEngine_OnImGuiRender;

            //The second way to create GUI is through the MonsterEngine.ImGui.Widgets namespace.
            // For now only the Text widget is fully operational.
            // To add a widget to the screen we just add it to the GUIManager, using GUIManager.AddItem.
            // The method returns a reference to the widget, the only thing left is to save it, and we can edit it every frame.
            m_text = GUIManager.AddItem(new Text("gui.text00", new Vector2(10, 10), new ColorV("05C0D4"),
                "Press Left Alt to move!\n" +
                "Press ESC to exit!\n" +
                "Press 1 for GUI.\n" +
                "Press 2 for Audio.", "arial")) as Text;
            
        }

        private void MonsterEngine_OnImGuiRender(object sender, EventArgs e)
        {
            //ImGui.Begin is used to render a "window" on the screen.
            // Without it no widgets will be shown.
            ImGui.Begin("Window");
            {
                //Displays a text in the window.
                ImGui.Text("This is a text, rendered by OnImGuiRender!");

                //Displays a button in the window.
                // If the button is clicked - it returns true.
                // In this instance we increment a variable but it can be use for debugging, profiling, etc.
                if (ImGui.Button("Click me"))
                    m_timesClicked++;

                //We display the m_timesClicked variable on the window.
                ImGui.Text("The button was clicked " + m_timesClicked + " times.");
            }
            //ImGui.End is used to stop the rendering of the window.
            // Be aware that if a window is not closed a "Debug Window" will be shown. So if that happens, check your code.
            ImGui.End();

            //ImGui can set the position and size of new windows before creating them.
            // Here we pin the window to these coordinates.
            ImGui.SetNextWindowPos(new Vector2(450, 300));
            ImGui.Begin("Another window!");
            {
                ImGui.Text("This is another ImGui window. You can create a ton of these.");
            }
            ImGui.End();
        }

        protected override void StageStop()
        {
            //If you decide to use the OnImGuiRender event, you must unbind it when the stage is stopped.
            Globals.MonsterEngine.OnImGuiRender -= MonsterEngine_OnImGuiRender;
        }

        protected override void StageUpdate()
        {
            if (Globals.MonsterEngine.IsKeyPressed(KeyboardKey.Escape))
                Globals.MonsterEngine.StopEngine();
            //When the '2' key is pressed, the stage will be changed to "stages.audioStage".
            // There are other methods for changing stages. Look at the Engine class.
            if (Globals.MonsterEngine.IsKeyPressed(KeyboardKey.D2))
                Globals.MonsterEngine.SetStage("stages.audioStage");

            //We saved a reference to the text, so now we can edit it's properties.
            // We will change it's color with a random color.
            // However the Update is too fast, so we will change it every 128th frame.
            if (m_cooldown == 128)
            {
                m_text.Color = ColorV.GetRandomColor(Globals.RandomNumberGenerator);
                m_cooldown = -1;
            }

            m_cooldown++;

            //This function is the same as DoFreeCamMovementWithoutKeyPress but the camera is moved only when the Left Alt (in this case) is pressed.
            // The last parameter is whether the cursor should be hidden and grabbed (forbidden to leave the window). 
            DoFreeCamMovementWithKeyPress(1.5f, 0.2f, Globals.MonsterEngine.IsKeyPressed(KeyboardKey.LeftAlt), true);
        }
    }
}
