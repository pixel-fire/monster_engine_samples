﻿using System;
using System.Collections.Generic;

using MonsterEngine.Engine;
using MonsterEngine.Engine.Settings;

using Sample04_Managers.Stages;

namespace Sample04_Managers
{
    class Program
    {
        static void Main(string[] args)
        {
            //Same code as in Sample00

            WindowSettings windowSettings = new WindowSettings(1600, 900, "Example 05 - Managers", false);
            GraphicsSettings graphicsSettings = new GraphicsSettings(false, 100, 100);
            EngineSettings engineSettings = new EngineSettings(true, false);
            //The only change in this example is that we add "audio.pak" to the engine.
            // See AudioStage.cs!
            PluginsSettings pluginsSettings = new PluginsSettings(false, false, true, new List<string>()
            {
                "fonts.pak",
                "shaders.pak",
                "audio.pak"
            });

            MasterSettings masterSettings = new MasterSettings(windowSettings, graphicsSettings, engineSettings, pluginsSettings);

            Engine engine = new Engine(masterSettings);

            //Protip: First see GUIStage.cs, than AudioStage.cs

            engine.StartEngine(new List<Stage>()
            {
                new GUIStage(),
                new AudioStage()
            });
        }
    }
}
