﻿using System;
using System.Collections.Generic;
using System.Text;

using MonsterEngine.Engine;
using MonsterEngine.Input;
using MonsterEngine.Structs;

using Sample01_Entities.Entities;

namespace Sample01_Entities.Stages
{
    class MainStage : Stage
    {
        public MainStage()
            :base("MainStage", "stages.mainStage")
        {

        }

        protected override void StageStart(string[] args = null)
        {
            //Adding a custom entity is as simple as adding a primitive.
            // 1. We must declare a class of type Entity/StaticEntity/MovableEntity.
            // 2. We add a new instance of this class to the stage.

            //See Entities/ExampleEntity.cs!

            var exampleEntity = AddEntity("exampleEntity", new ExampleEntity(this));
            exampleEntity.Transform.SetScale(new Vector3(0.2f));
        }

        protected override void StageStop()
        {
            
        }

        protected override void StageUpdate()
        {
            //For the update method, we copy it from Sample00

            if (Globals.MonsterEngine.IsKeyPressed(KeyboardKey.Escape))
                Globals.MonsterEngine.StopEngine();

            DoFreeCamMovementWithoutKeyPress();
        }
    }
}
