﻿using MonsterEngine.Components;
using MonsterEngine.Engine;
using MonsterEngine.Entities;
using MonsterEngine.Enums;
using MonsterEngine.Primitives3D;
using MonsterEngine.Structs;

namespace Sample01_Entities.Entities
{
    //For a custom entity we declare a class and inherit either Entity, StaticEntity or MovableEntity
    //  *Entity is a class, which implements IEntity and adds a Quality Of Life methods.
    //    It is primarily used for entities that are both Movable and Static.
    //  *StaticEntity is an Entity, which will not be updated, only renderer. Eg. Buildings, props.
    //  *MovableEntity is an Entity, which will be updated every frame. Eg. NPCs, Doors.
    class ExampleEntity : Entity
    {
        //The constructor should take the parent stage as a parameter.
        public ExampleEntity(Stage parent)
            :base(parent)
        {
            //This function is used to define the Render and Update methods.
            //  If using a StaticEntity it is not recommended to use this function.
            UpdateBehaviour(Update, Render);

            //Here we create the material for the entity.
            // Materials can be two types:
            //  1. Simple Materials - Defuse and Specular textures, specular value and a shine value. (Simple and Advanced Shader)
            //  2. PBR Materials - Albedo(Base), Normal, Metallic, Roughness and Ambient Occlusion textures. (PBR Shader)
            var defuseTexture = Globals.MonsterEngine.Settings.GetPakFile("default").GetTextureByName("cube_def").Texture;
            var specularTexture = Globals.MonsterEngine.Settings.GetPakFile("default").GetTextureByName("cube_spec").Texture;

            Material material = new Material(new Vector3(5), 32f, defuseTexture, specularTexture);

            //AddComponent is function, which adds a component to the entity.
            // Components are modifiers to the entity. They help with it's functionality.
            // Eg. MeshRenderer - renders a 3D mesh; BoxCollider - provides collision and physics.
            AddComponent(new MeshRenderer(this, PrimitiveMesh.Cylinder, material, ShaderUsed.SimpleLit));
        }

        //This is the render method, here is recommended to use only RenderComponents.
        void Render()
        {
            RenderComponents();
        }

        //This is the update method, here is recommended to use UpdateComponents before any other logic.
        void Update()
        {
            UpdateComponents();

            //The engine comes with a pre-build Random Number Generator(Globals.RandomNumberGenerator).
            // So in this example we generate a number between 0.00f and 1.00f and
            //  if the number is [0; 0.5], we scale the model a little bit,
            //  else we rotate it a little bit.
            if (Globals.RandomNumberGenerator.NextDouble() <= 0.5f)
                Transform.AddScale(Axis.XYZ, 0.05f * Globals.DeltaTime);
            else
                Transform.AddRotation(Axis.XYZ, 5f * Globals.DeltaTime);
        }
    }
}
