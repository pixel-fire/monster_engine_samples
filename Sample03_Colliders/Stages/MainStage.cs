﻿using System;
using System.Collections.Generic;
using System.Text;

using MonsterEngine.Components;
using MonsterEngine.Engine;
using MonsterEngine.Enums;
using MonsterEngine.Input;
using MonsterEngine.Primitives3D;
using MonsterEngine.Structs;

namespace Sample03_Colliders.Stages
{
    class MainStage : Stage
    {
        public MainStage()
            : base("MainStage", "stages.MainStage")
        {

        }

        protected override void StageStart(string[] args = null)
        {
            var texture_cube_def = Globals.MonsterEngine.Settings.GetPakFile("default").GetTextureByName("cube_def").Texture;
            var texture_cube_spec = Globals.MonsterEngine.Settings.GetPakFile("default").GetTextureByName("cube_spec").Texture;

            float x = 0;
            float y = 0;
            float z = 0;

            //RigidCube cube;

            for (int i = 0; i < 20; i++)
            {
                x = (float)(Globals.RandomNumberGenerator.Next(-15, 15) + Globals.RandomNumberGenerator.NextDouble());
                y = (float)(Globals.RandomNumberGenerator.Next(10, 20) + Globals.RandomNumberGenerator.NextDouble());
                z = (float)(Globals.RandomNumberGenerator.Next(-15, 15) + Globals.RandomNumberGenerator.NextDouble());

                ConsoleMessage.ToConsole(MessageType.Message, "MainStage.StageStart(string[])", "CubeID: " + i + "; X: " + x + "; Z: " + z);

                var cube = AddEntity("rigidCube_" + i, new Cube(this));
                cube.Transform.SetPosition(new Vector3(x, y, z));
                cube.SetMaterial(new Material(new Vector3(0.5f), 32f, texture_cube_def, texture_cube_spec));

                cube.AddComponent(new RigidBody(cube, "rigidCube_" + i, cube.Transform.Scale));
                cube.GetComponent<RigidBody>().UseGravity = true;
                cube.GetComponent<RigidBody>().Mass = Globals.RandomNumberGenerator.Next(50, 400);
            }

            var floor = AddEntity("floor", new Cube(this));
            floor.Transform.SetPosition(new Vector3(0, -8, 0));
            floor.Transform.SetScale(new Vector3(50, 1f, 50));
            floor.AddComponent(new RigidBody(floor, "floor_collider", floor.Transform.Scale));
            //Should the entity start to fall when the stage is running?
            floor.GetComponent<RigidBody>().UseGravity = false;
            //Is the entity immovable?
            floor.GetComponent<RigidBody>().IsStatic = true;
            //How much is the weight of the entity in kg.
            floor.GetComponent<RigidBody>().Mass = 10000;
        }

        protected override void StageStop()
        {
            
        }

        protected override void StageUpdate()
        {
            if (Globals.MonsterEngine.IsKeyPressed(KeyboardKey.Escape))
                Globals.MonsterEngine.StopEngine();

            DoFreeCamMovementWithoutKeyPress();
        }
    }
}
