﻿using System.Collections.Generic;

using MonsterEngine.Engine;
using MonsterEngine.Engine.Settings;
using MonsterEngine.Enums;

using Sample00_SettingUp.Stages;

namespace Sample00_SettingUp
{
    class Program
    {
        static void Main(string[] args)
        {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            //  ====In order to start the engine we need a couple of things done first.====
            //
            //  1. Make sure that the project has these nuget packages: System.Drawing.Common; System.Runtime.CompilerServices.Unsafe.
            //     Also make sure before starting the application that the files form the "Required" folder are in the build path.
            //     If the application is using audio, install the openALInstaller.exe from the "Install" folder.
            //
            //  2. Set the MasterSettings structure - this can be done 2 ways:
            //      *By using the constructor and setting every setting manually;
            //      *By making 4 settings structures, setting their values and than passing them to the MasterSettings.
            //  Both ways are the same, you choose which to use.
            //
            //  3. Initialize the engine using the master settings.
            //
            //  4. Start the engine with the list of all stages.
            //
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //The WindowSettings is used to define the behavior of the window.
            WindowSettings windowSettings = new WindowSettings(1600, 900, "Example 00 - Setting Up", false);

            //The GraphicsSettings is used to define the graphical capabilities of the engine.
            //  Remarks:
            //      *If the first parameter is true nothing will be show on to the screen!
            //      *The only valid value for the last parameter is "GFXBackend.OpenGL"!
            GraphicsSettings graphicsSettings = new GraphicsSettings(false, 100, 100, 
                AntiAliasingMode.MSAA, 4, true, 4, GFXBackend.OpenGL);

            //The EngineSettings is used to define the behavior of the engine.
            //  Remarks:
            //      *If the first parameter is true - every light and audio source will be shown!
            //      *If the second is true - at the start of the engine will be a credit screen which will last for 5 seconds.
            //      *The only valid value for the last parameter is "ApplicationMode.App3D"!
            EngineSettings engineSettings = new EngineSettings(true, false, ApplicationMode.App3D);

            //The PluginsSettings is used to define should the engine use plug-ins.
            //  Remarks:
            //      *For now the first and second parameters need to be set to false. They are under development :)
            //      *The third parameter has to be true, although it is not necessary. If it's false, than the last parameter should be null.
            //      *The last parameter is a List<string> in which are written the names of the packages that will be used in the
            //          engine with ".pak" at the end.
            PluginsSettings pluginsSettings = new PluginsSettings(false, false, true, new List<string>()
            {
                "shaders.pak",
                "fonts.pak"
            });

            //The MasterSetting is a type of a container for all engine settings.
            MasterSettings masterSettings = new MasterSettings(windowSettings, graphicsSettings, engineSettings, pluginsSettings);

            //The engine object. The most crucial part of a working MonsterEngine application.
            Engine engine = new Engine(masterSettings);

            //ConsoleMessage is a built-in static class which prints to the console formated messages.
            // It also saves the logs of all messages to a file, when the engine is stopped.
            // It is recommended to use it instead of Console.WriteLine();
            ConsoleMessage.ToConsole(MessageType.Message, "Main(string[])", "Hello, Monster Engine!");

            //StartEngine is used to start the engine. 
            // If the showCredits setting is true it will show it first, otherwise, it will show the first stage in the list.
            engine.StartEngine(new List<Stage>()
            {
                new MainStage()
            });
        }
    }
}
