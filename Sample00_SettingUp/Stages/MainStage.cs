﻿using MonsterEngine.Engine;
using MonsterEngine.Enums;
using MonsterEngine.Input;
using MonsterEngine.Primitives3D;
using MonsterEngine.Structs;

namespace Sample00_SettingUp.Stages
{
    //This is a stage.
    // The main part of the engine, here happens all the magic.
    // First of all, you must declare a class, which inherits the Stage class.
    class MainStage : Stage
    {
        Cube cube;

        public MainStage()
            //The parent's constructor.
            // The name can be anything, however the id should be in this format: stages.xxxxx
            // xxxxx -> the identifier of the stage.
            : base("Main Stage", "stages.mainStage")
        {

        }

        //This method is called when the stage is firstly started.
        // Primarily used for loading settings, initializing materials, entities, etc.
        protected override void StageStart(string[] args)
        {
            //The AddEntity function is used for adding entities to the stage.
            // The only thing necessary is:
            //  *A stage id - a unique identifier of the entity.
            //  *An entity - the entity type to add.
            // This function returns a reference to the added entity.
            // So you can edit it's position, rotation, scale, etc.

            var floor = AddEntity("floor", new Cube(this));
            floor.Transform.SetPosition(new Vector3(0, -2, 0));
            floor.Transform.SetRotation(new Vector3(0, 0, 0));
            floor.Transform.SetScale(new Vector3(10, 0.25f, 10));

            //All primitives have events for when the entity is disabled/enabled, rendered and updated.
            // You can listen to them and edit the primitive's behavior.
            cube = AddEntity("cube", new Cube(this));
            cube.Transform.SetPosition(new Vector3(0, 3, 0));
            cube.OnUpdate += Cube_OnUpdate;
        }

        private void Cube_OnUpdate(object sender, System.EventArgs e)
        {
            //So each frame the cube will be rotated with 15 degrees on the Z axis.
            // We multiply with Globals.DeltaTime to ensure that it will rotate with the same speed on all computers.
            cube.Transform.AddRotation(Axis.Z, 15 * Globals.DeltaTime);
        }

        //This method is called when the stage is stopped.
        // Primarily used for unloading data.
        protected override void StageStop()
        {
            
        }

        //This method is called every frame.
        // Primarily used for updating entities and data.
        protected override void StageUpdate()
        {
            //This line checks if the escape key is pressed and stops the engine.
            if (Globals.MonsterEngine.IsKeyPressed(KeyboardKey.Escape))
                Globals.MonsterEngine.StopEngine();

            //This is a prebuilt function for a free camera.
            DoFreeCamMovementWithoutKeyPress();
        }
    }
}
